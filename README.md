# Para executar o projeto
#### **Entre na pasta do projeto e rode os comandos:**

###### instale as dependências necessárias com:

* pip3 install -r requirements.txt

### Para subir o container use:

``` docker build . -t portal```
``` docker run -p 5432:5432 --name="portal" -v local_db:/var/lib/postgresql/data portal```

###### Para fazer a migração do banco de dados execute:

``` python3 manage.py makemigrations mainapp```
``` python3 manage.py migrate```

###### Para rodar os testes execute:

``` python3 manage.py test ```

###### Crie um usuário administrador com:

``` python3 manage.py createsuperuser```

###### Para executar iniciar o servidor utilize:

``` python3 manage.py runserver```
