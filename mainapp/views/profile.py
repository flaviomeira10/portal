from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from django.utils.translation import ugettext_lazy as _
from mainapp.forms import CustomUserChangeForm, CustomUserCreationForm


@login_required
def update_user(request):
    """
    Função para edição de usuário.
    """
    if request.method == 'POST':
        user_form = CustomUserChangeForm(request.POST, instance=request.user)
        if user_form.is_valid():
            user_form.save()
            messages.success(request, _('Perfil editado!'))
            return redirect('index')
        else:
            messages.error(request, _('Corrija o seguinte erro.'))
    else:
        user_form = CustomUserChangeForm(instance=request.user)
    return render(request, 'edit_user.html', {
        'user_form': user_form,
    })


def create_user(request):
    """
    Rota de criação de usuários
    """
    if request.method == 'POST':
        user_form = CustomUserCreationForm(request.POST)
        if user_form.is_valid():
            user_form.save()
            messages.success(request, _('Perfil criado!'))
            username = user_form.cleaned_data.get('username')
            raw_password = user_form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('index')
    form = CustomUserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})
