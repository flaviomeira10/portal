"""Funções de movimentação nas tabelas do banco."""

from mainapp.models import Comment, Publication, CommentLike, PublicationLike
from mainapp.forms import EvaluationForm


def register_pub_evaluation(obj: Publication, request):
    """
    Registra uma avaliação (like ou dislike) caso o usuário não tenha avaliado a publicação,
    se o usuário já avaliou, o estado da avaliação será alterado de acordo com o valor do POST.
    Caso o resultado da avaliação seja o mesmo presente no banco de dados, ela deixa de existir.

    Args:
        obj: publicação que receberá a avaliação.
        request: contexto da requisição recebida.

    Returns:
        Caso a avaliação seja exatamente a mesma que existe no banco de dados,
    retorna o objeto deletado.
        Caso a avaliação exista mas seja alterada, retorna o objeto com o novo valor.
        Caso a avalição não exista, retorna a nova avaliação
    """
    existing_evaluation = PublicationLike.objects.filter(target=obj, user=request.user)
    if existing_evaluation:
        if request.POST['positive'] and existing_evaluation[0].positive:
            return existing_evaluation.delete()
        evaluation = existing_evaluation[0]
        evaluation.positive = not evaluation.positive
        return evaluation.save()
    pub = PublicationLike(target=obj,
                          user=request.user,
                          positive=request.POST['positive'])
    return pub.save()


def register_comment_evaluation(obj: Comment, request):
    """
    Registra uma avaliação (like ou dislike) caso o usuário não tenha avaliado o comentário,
    se o usuário já avaliou, o estado da avaliação será alterado de acordo com o valor do POST.
    Caso o resultado da avaliação seja o mesmo presente no banco de dados, ela deixa de existir.

    Args:
        obj: publicação que receberá a avaliação.
        request: contexto da requisição recebida.

    Returns:
        Caso a avaliação seja exatamente a mesma que existe no banco de dados,
    retorna o objeto deletado.
        Caso a avaliação exista mas seja alterada, retorna o objeto com o novo valor.
        Caso a avalição não exista, retorna a nova avaliação
    """
    existing_evaluation = CommentLike.objects.filter(target=obj, user=request.user).first()
    if existing_evaluation:
        evaluation_status = existing_evaluation.positive
        form = EvaluationForm(request.POST, instance=existing_evaluation)
        if form.is_valid():
            if form.cleaned_data['positive'] == evaluation_status:
                return existing_evaluation.delete()
            return form.save()
    form = EvaluationForm(request.POST)
    evaluation = form.save(commit=False)
    evaluation.user = request.user
    evaluation.target = obj
    return evaluation.save()
