from django.shortcuts import get_object_or_404, render
from mainapp.forms import CommentForm
from mainapp.models import Comment, Publication, Tag


def index(request):
    """Homepage."""
    pub_list = Publication.objects.all()
    return render(request, 'index.html')


def publication(request, publication_id):
    """
    Renderiza o template da publicação com as informações necessárias.
    """
    pub = get_object_or_404(Publication, pk=publication_id)
    tags = Tag.objects.filter(publication=pub)
    comments = Comment.objects.filter(publication=pub)
    return render(request, 'publication.html', {'pub': pub,
                                                'comments': comments,
                                                'tags': ', '.join(t.tag for t in tags),
                                                'comment_form': CommentForm()})


def publication_list(request):
    """
    Função para listar publicações.
    """
    pub_list = Publication.objects.all()
    return render(request, 'publication_list.html', {'pubs': pub_list})
