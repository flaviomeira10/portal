from django.test import TestCase
from django.utils import timezone
from mainapp import models


class PublicationTests(TestCase):
    publication_data = {
        'title': 'testpub',
        'pub_date': timezone.now(),
        'text': 'test publication'
    }

    def setUp(self):
        self.user = models.CustomUser.objects.create_user(username='testuser', password='secret')
        self.area = models.Area.objects.create(name='testarea', admin=self.user)
        self.pub_type = models.PublicationType.objects.create(text='test')
        self.publication_data['user'] = self.user
        self.publication_data['pub_type'] = self.pub_type
        self.pub = models.Publication.objects.create(**self.publication_data)

    def test_publication(self):
        response = self.client.get(f'/publications/{self.pub.pk}')
        self.assertEqual(response.status_code, 200)

    def test_publication_returns_404_at_invalid_index(self):
        response = self.client.get('/publications/x')
        self.assertEqual(response.status_code, 404)

    def test_publication_renders_right_template(self):
        response = self.client.get('/publications/1')
        self.assertTemplateUsed('publications.html')
