"""Testes dos comentários"""

from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

from mainapp import forms
from mainapp import models


class CommentTests(TestCase):
    publication_data = {
        'title': 'testpub',
        'pub_date': timezone.now(),
        'text': 'test publication'
    }

    def setUp(self):
        self.user = models.CustomUser.objects.create_user(username='testuser', password='secret')
        self.area = models.Area.objects.create(name='testarea', admin=self.user)
        self.pub_type = models.PublicationType.objects.create(text='test')
        self.publication_data['user'] = self.user
        self.publication_data['pub_type'] = self.pub_type
        self.pub = models.Publication.objects.create(**self.publication_data)
        self.comment = models.Comment.objects.create(
            publication=self.pub,
            author=self.user,
            comment_text='test comment',
            pub_date=timezone.datetime(day=1, month=1, year=2018),
        )

    def test_comment_exists(self):
        """Valida a existencia do comentário."""
        bd_comment = models.Comment.objects.first()
        self.assertEqual(bd_comment, self.comment)

    def test_new_comment_route_with_authenticated_session(self):
        """Valida a criação de um comentário com usuário autenticado"""
        form = forms.CommentForm(data={'comment_text': 'i am a new comment!'})
        self.client.login(username='testuser', password='secret')
        response = self.client.post(reverse('new_comment', args=(self.pub.pk,)),
                                    data=form.data)
        last_comment = models.Comment.objects.last()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(len(models.Comment.objects.all()), 2)
        self.assertEqual(last_comment.comment_text, 'i am a new comment!')

    def test_new_comment_route_without_authentication(self):
        """Valida a falha na criação do comentário sem usuário autenticado."""
        form = forms.CommentForm(data={'comment_text': 'i am a new comment!'})
        response = self.client.post(reverse('new_comment', args=(self.pub.pk,)),
                                    data=form.data)
        last_comment = models.Comment.objects.last()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(len(models.Comment.objects.all()), 1)
        self.assertNotEqual(last_comment.comment_text, 'i am a new comment!')

    def test_edit_comment_with_authenticated_session(self):
        """Valida a edição de um comentário com usuario autenticado."""
        self.client.login(username='testuser', password='secret')
        response = self.client.post(reverse('edit_comment', args=(self.comment.pk,)),
                                    data={'comment_text': 'this comment was edited!'})
        self.assertEqual(response.status_code, 200)
        comment = models.Comment.objects.first()
        self.assertEqual(comment.comment_text, 'this comment was edited!')
        
    def test_edit_comment_without_authentication(self):
        """Valida a falha da edição de um comentário sem usuário autenticado."""
        response = self.client.post(reverse('edit_comment', args=(self.comment.pk,)),
                                    data={'comment_text': 'this comment was edited!'})
        self.assertEqual(response.status_code, 302)
        comment = models.Comment.objects.first()
        self.assertNotEqual(comment.comment_text, 'this comment was edited!')

    def test_like_comment_once(self):
        """Valida o like no comentário uma vez"""
        self.client.login(username='testuser', password='secret')
        response = self.client.post(reverse('evaluate_comment', args=(self.comment.pk,)),
                                    data={'positive': True})
        self.assertEqual(response.status_code, 200)
        evaluation = models.CommentLike.objects.get(target=self.comment)
        self.assertEqual(evaluation.positive, True)

    def test_like_comment_twice(self):
        """Valida o like no comentário duas vezes"""
        self.client.login(username='testuser', password='secret')
        response1 = self.client.post(reverse('evaluate_comment', args=(self.comment.pk,)),
                                     data={'positive': True})
        response2 = self.client.post(reverse('evaluate_comment', args=(self.comment.pk,)),
                                     data={'positive': True})
        self.assertEqual(response1.status_code, 200)
        self.assertEqual(response2.status_code, 200)
        self.assertEqual(len(models.CommentLike.objects.all()), 0)

    def test_like_comment_then_dislike_it(self):
        """Valida o comportamento quando o usuário dá o like em seguida o dislike."""
        self.client.login(username='testuser', password='secret')
        response1 = self.client.post(reverse('evaluate_comment', args=(self.comment.pk,)),
                                     data={'positive': True})
        response2 = self.client.post(reverse('evaluate_comment', args=(self.comment.pk,)),
                                     data={'positive': False})
        self.assertEqual(response1.status_code, 200)
        self.assertEqual(response2.status_code, 200)
        evaluation = models.CommentLike.objects.get(target=self.comment)
        self.assertEqual(evaluation.positive, False)

    def test_dislike_comment_once(self):
        """Valida a adição de um dislike"""
        self.client.login(username='testuser', password='secret')
        response = self.client.post(reverse('evaluate_comment', args=(self.comment.pk,)),
                                    data={'positive': False})
        self.assertEqual(response.status_code, 200)
        evaluation = models.CommentLike.objects.get(target=self.comment)
        self.assertEqual(evaluation.positive, False)

    def test_dislike_comment_twice(self):
        """Valida a adição de um dislike e depois sua deleção"""
        self.client.login(username='testuser', password='secret')
        response1 = self.client.post(reverse('evaluate_comment', args=(self.comment.pk,)),
                                     data={'positive': False})
        response2 = self.client.post(reverse('evaluate_comment', args=(self.comment.pk,)),
                                     data={'positive': False})
        self.assertEqual(response1.status_code, 200)
        self.assertEqual(response2.status_code, 200)
        self.assertEqual(len(models.CommentLike.objects.all()), 0)
