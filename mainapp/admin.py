from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import Area, Comment, Publication, PublicationType, Tag, CustomUser
from .forms import CustomUserChangeForm, CustomUserCreationForm


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ['email', 'username']


admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Area)
admin.site.register(Publication)
admin.site.register(PublicationType)
admin.site.register(Comment)
admin.site.register(Tag)
