from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _


class CustomUser(AbstractUser):
    """Tabela de usuários."""
    birthdate = models.DateField("Data de nascimento", default=None, blank=True, null=True)

    def __str__(self):
        return self.email


class Area(models.Model):
    """Tabela de áreas."""
    name = models.CharField(_("Nome"), max_length=200)
    admin = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Community(models.Model):
    """Tabela de comunidades."""
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    area = models.ForeignKey(Area, on_delete=models.CASCADE)

    def __str__(self):
        return self.area


class PublicationType(models.Model):
    """Tabela de tipos de publicação."""
    text = models.CharField(max_length=50)

    def __str__(self):
        return self.text


class Publication(models.Model):
    """Tabela de publicações."""
    title = models.CharField(_("Título da publicação."), max_length=200)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    pub_date = models.DateTimeField("data da publicação.", auto_now_add=True)
    pub_type = models.ForeignKey(PublicationType, on_delete=models.CASCADE)
    text = models.TextField(_("Texto da publicação."))

    def __str__(self):
        return self.title


class Tag(models.Model):
    """Tabela de tags."""
    tag = models.CharField(max_length=50)
    publication = models.ForeignKey(Publication, on_delete=models.CASCADE)


class Comment(models.Model):
    """Tabela de comentários."""
    publication = models.ForeignKey(Publication, on_delete=models.CASCADE)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    comment_text = models.TextField(_("Texto do comentário."))
    pub_date = models.DateTimeField("Data do comentário.", auto_now_add=True)
    response_to = models.ForeignKey('self', on_delete=models.CASCADE, null=True, default=None)

    def __str__(self):
        text = self.comment_text if len(self.comment_text) <= 30 else self.comment_text[:30]
        return f"{text}..."


class CommentLike(models.Model):
    """Tabela de likes e dislikes da publicação."""
    target = models.ForeignKey(Comment, on_delete=models.CASCADE)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    positive = models.BooleanField()


class PublicationLike(models.Model):
    """Tabela de likes e dislikes da publicação"""
    target = models.ForeignKey(Publication, on_delete=models.CASCADE)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    positive = models.BooleanField()
