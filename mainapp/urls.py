from django.urls import path

from .views import comments, index, profile

urlpatterns = [
    path('', index.index, name='index'),
    path('publications', index.publication_list, name='publication_list'),
    path('publications/<int:publication_id>', index.publication, name='publication'),
    path('comments/new_comment/<int:pk>', comments.new_comment, name='new_comment'),
    path('comments/edit_comment/<int:pk>', comments.edit_comment, name='edit_comment'),
    path('comments/evaluate_comment/<int:pk>', comments.evaluate_comment, name='evaluate_comment'),
    path('user/update', profile.update_user, name='update_user'),
    path('user/create', profile.create_user, name='create_user')
]
