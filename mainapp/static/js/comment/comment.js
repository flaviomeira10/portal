document.addEventListener("DOMContentLoaded", function() {
  var elems = document.querySelectorAll(".modal");
  var instances = M.Modal.init(elems);
});

$(".modal-button-ok").click(function() {
  var id = $(this).attr("id");
  var modal = document.getElementById("modal" + id);
  var instance = M.Modal.getInstance(modal);
  var new_comment = [...modal.getElementsByTagName("input")][0];
  data = {
    comment_text: new_comment.value,
    csrfmiddlewaretoken: csrftoken
  };
  $.ajax({
    url: "/comments/edit_comment/" + id,
    cache: "false",
    type: "POST",
    data: data,
    success: function(){
        instance.close();
        M.toast(success_toaster)
        document.getElementById("comment-" + id).innerHTML = new_comment.value;
    },
    error: function(){
        instance.close()
        M.toaster(error_toaster)
    }
  });
});

success_toaster = {
    displayLength: 2000,
    html: 'Sucesso!', 
    classes: 'green lighten-2'
}

error_toaster = {
    displayLength: 2000,
    html: 'Falha na comunicação com o servidor!', 
    classes: 'red lighten-2'
}

$(".cmt-up").click(function() {
  var id = $(this).attr("id");
  data = {
    positive: true
  };
  $.ajax({
    url: "comments/evaluate_comment/" + id.split("-")[1],
    cache: "false",
    type: "POST",
    data: data,
    success: function(){
        document.getElementById(id).toggleAttribute("liked")
    },
    error: function(){
        M.toaster(error_toaster)
    }
  });
});

$(".cmt-down").click(function() {
  var id = $(this).attr("id");
  data = {
    positive: true
  };
  $.ajax({
    url: "comments/evaluate_comment/" + id.split("-")[1],
    cache: "false",
    type: "POST",
    data: data,
    success: function(){
        document.getElementById(id).toggleAttribute("liked")
    },
    error: function(){
        M.toaster(error_toaster)
    }
  });
});